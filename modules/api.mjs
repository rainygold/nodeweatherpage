// contains all logic for directly interacting with the OpenWeatherAPI

// imports
import fetch from 'node-fetch';
import babelPolyfill from 'babel-polyfill';

// module variables
const apiKey = 'f81821b468dd7d30e5a64dac8a72205a';

// module methods

// fetches the weather data for the input city
export async function fetchWeatherData(city) {

    // grab the relevant data from the API
    let response = await fetch('http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&mode=json&appid=' + apiKey);

    // convert to json
    let data = await response.json();

    // return the data to be handled elsewhere
        return data;


};