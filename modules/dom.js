// handles all logic for displaying weather data on the web page

// appends relevant data to each div
function displayWeatherData(json) {

    // validate the data
    if (json == undefined || json.list == undefined) {
        window.alert('City not found. Please try another!');
        return false;
    } else {

        // set up divs for manipulation
        var htmlDivs = getCollOfElements();

        // call sort function
        var sortedData = sortJsonIntoDays(json);

        // insert the data on the page
        appendDataToPage(htmlDivs, sortedData);
    }
}

// appends information to the relevant div
function appendDataToPage(divs, data) {

    // append all of the data to each day div

    // for loop through each object's arrays
    for (var i = 0; i < data.dayOne.temps.length; i++) {

        // add line breaks between each date (time of day)
        var linebreak = document.createElement('br');
        var secondLinebreak = document.createElement('br');

        // use innerHTML to add the data to the page
        divs.dayOne.innerHTML += '<b>Time: </b>' + JSON.stringify(data.dayOne.time[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayOne.appendChild(linebreak);
        divs.dayOne.innerHTML += '<b>Temperature: </b>' + JSON.stringify(data.dayOne.temps[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayOne.appendChild(linebreak);

        // not every date/time has wind data
        if (data.dayOne.wind.length > 0) {
            divs.dayOne.innerHTML += '<b>Wind speed: </b>' + JSON.stringify(data.dayOne.wind[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
            divs.dayOne.appendChild(linebreak);
        }

        divs.dayOne.innerHTML += '<b>Weather: </b>' + JSON.stringify(data.dayOne.weather[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        document.getElementById('day-1-container').appendChild(linebreak);
        document.getElementById('day-1-container').appendChild(secondLinebreak);
    }

    // day two
    for (var i = 0; i < data.dayTwo.temps.length; i++) {

        // add line breaks between each date (time of day)
        var linebreak = document.createElement('br');
        var secondLinebreak = document.createElement('br');

        // use innerHTML to add the data to the page
        divs.dayTwo.innerHTML += '<b>Time: </b>' + JSON.stringify(data.dayTwo.time[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayTwo.appendChild(linebreak);
        divs.dayTwo.innerHTML += '<b>Temperature: </b>' + JSON.stringify(data.dayTwo.temps[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayTwo.appendChild(linebreak);

        // not every date/time has wind data
        if (data.dayTwo.wind.length > 0) {
            divs.dayTwo.innerHTML += '<b>Wind speed: </b>' + JSON.stringify(data.dayTwo.wind[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
            divs.dayTwo.appendChild(linebreak);
        }

        divs.dayTwo.innerHTML += '<b>Weather: </b>' + JSON.stringify(data.dayTwo.weather[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        document.getElementById('day-2-container').appendChild(linebreak);
        document.getElementById('day-2-container').appendChild(secondLinebreak);
    }

    // day three
    for (var i = 0; i < data.dayThree.temps.length; i++) {

        // add line breaks between each date (time of day)
        var linebreak = document.createElement('br');
        var secondLinebreak = document.createElement('br');

        // use innerHTML to add the data to the page
        divs.dayThree.innerHTML += '<b>Time: </b>' + JSON.stringify(data.dayThree.time[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayThree.appendChild(linebreak);
        divs.dayThree.innerHTML += '<b>Temperature: </b>' + JSON.stringify(data.dayThree.temps[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayThree.appendChild(linebreak);

        // not every date/time has wind data
        if (data.dayThree.wind.length > 0) {
            divs.dayThree.innerHTML += '<b>Wind speed: </b>' + JSON.stringify(data.dayThree.wind[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
            divs.dayThree.appendChild(linebreak);
        }

        divs.dayThree.innerHTML += '<b>Weather: </b>' + JSON.stringify(data.dayThree.weather[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        document.getElementById('day-3-container').appendChild(linebreak);
        document.getElementById('day-3-container').appendChild(secondLinebreak);
    }

    // day four
    for (var i = 0; i < data.dayFour.temps.length; i++) {

        // add line breaks between each date (time of day)
        var linebreak = document.createElement('br');
        var secondLinebreak = document.createElement('br');

        // use innerHTML to add the data to the page
        divs.dayFour.innerHTML += '<b>Time: </b>' + JSON.stringify(data.dayFour.time[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayFour.appendChild(linebreak);
        divs.dayFour.innerHTML += '<b>Temperature: </b>' + JSON.stringify(data.dayFour.temps[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayFour.appendChild(linebreak);

        // not every date/time has wind data
        if (data.dayFour.wind.length > 0) {
            divs.dayFour.innerHTML += '<b>Wind speed: </b>' + JSON.stringify(data.dayFour.wind[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
            divs.dayFour.appendChild(linebreak);
        }

        divs.dayFour.innerHTML += '<b>Weather: </b>' + JSON.stringify(data.dayFour.weather[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        document.getElementById('day-4-container').appendChild(linebreak);
        document.getElementById('day-4-container').appendChild(secondLinebreak);
    }

    // day five
    for (var i = 0; i < data.dayFive.temps.length; i++) {

        // add line breaks between each date (time of day)
        var linebreak = document.createElement('br');
        var secondLinebreak = document.createElement('br');

        // use innerHTML to add the data to the page
        divs.dayFive.innerHTML += '<b>Time: </b>' + JSON.stringify(data.dayFive.time[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayFive.appendChild(linebreak);
        divs.dayFive.innerHTML += '<b>Temperature: </b>' + JSON.stringify(data.dayFive.temps[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        divs.dayFive.appendChild(linebreak);

        // not every date/time has wind data
        if (data.dayFive.wind.length > 0) {
            divs.dayFive.innerHTML += '<b>Wind speed: </b>' + JSON.stringify(data.dayFive.wind[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
            divs.dayFive.appendChild(linebreak);
        }

        divs.dayFive.innerHTML += '<b>Weather: </b>' + JSON.stringify(data.dayFive.weather[i]).replace(/^\[|]$/g, '').replace(/\'|#/g, '');
        document.getElementById('day-5-container').appendChild(linebreak);
        document.getElementById('day-5-container').appendChild(secondLinebreak);
    }

}

// colelcts all necessary html elements into one object for easier access
function getCollOfElements() {

    // assign vars to each day 'slot' on the page
    var dayOneDiv = document.getElementById('day-1-container');

    var dayTwoDiv = document.getElementById('day-2-container');

    var dayThreeDiv = document.getElementById('day-3-container');

    var dayFourDiv = document.getElementById('day-4-container');

    var dayFiveDiv = document.getElementById('day-5-container');

    // compact the vars into an object
    var collOfElements = {

        dayOne: dayOneDiv,

        dayTwo: dayTwoDiv,

        dayThree: dayThreeDiv,

        dayFour: dayFourDiv,

        dayFive: dayFiveDiv,
    };

    return collOfElements;
};

// sorts the json into separate objects for each day
function sortJsonIntoDays(json) {

    var collOfDates = new Set();

    // object to store all info for each day
    var dayObjects = {
        dayOne: {
            time: [],
            temps: [],
            weather: [],
            wind: [],
        },
        dayTwo: {
            time: [],
            temps: [],
            weather: [],
            wind: [],
        },
        dayThree: {
            time: [],
            temps: [],
            weather: [],
            wind: [],
        },
        dayFour: {
            time: [],
            temps: [],
            weather: [],
            wind: [],
        },
        dayFive: {
            time: [],
            temps: [],
            weather: [],
            wind: [],
        },
    };

    // grab all the dates
    for (var element in json.list) {
        // take off the time of day
        collOfDates.add(json.list[element].dt_txt.split(' ', 1).toString());
    };

    // convert Set to Array to use methods
    let arrayCollOfDates = Array.from(collOfDates);

    // grab all the temps for each date 
    arrayCollOfDates.forEach(date => {
        for (var listNo in json.list) {

            // dates match so push the relevant data into their appropriate date
            if (json.list[listNo].dt_txt.split(' ', 1).toString() === date) {
                switch (arrayCollOfDates.indexOf(date)) {
                    case 0:
                        dayObjects.dayOne.temps.push(json.list[listNo].main.temp);
                        dayObjects.dayOne.weather.push(json.list[listNo].weather[0].main);
                        dayObjects.dayOne.wind.push(json.list[listNo].wind.speed);
                        dayObjects.dayOne.time.push(json.list[listNo].dt_txt);
                        break;
                    case 1:
                        dayObjects.dayTwo.temps.push(json.list[listNo].main.temp);
                        dayObjects.dayTwo.weather.push(json.list[listNo].weather[0].main);
                        dayObjects.dayTwo.wind.push(json.list[listNo].wind.speed);
                        dayObjects.dayTwo.time.push(json.list[listNo].dt_txt);
                        break;
                    case 2:
                        dayObjects.dayThree.temps.push(json.list[listNo].main.temp);
                        dayObjects.dayThree.weather.push(json.list[listNo].weather[0].main);
                        dayObjects.dayThree.wind.push(json.list[listNo].wind.speed);
                        dayObjects.dayThree.time.push(json.list[listNo].dt_txt);
                        break;
                    case 3:
                        dayObjects.dayFour.temps.push(json.list[listNo].main.temp);
                        dayObjects.dayFour.weather.push(json.list[listNo].weather[0].main);
                        dayObjects.dayFour.wind.push(json.list[listNo].wind.speed);
                        dayObjects.dayFour.time.push(json.list[listNo].dt_txt);
                        break;
                    case 4:
                        dayObjects.dayFive.temps.push(json.list[listNo].main.temp);
                        dayObjects.dayFive.weather.push(json.list[listNo].weather[0].main);
                        dayObjects.dayFive.wind.push(json.list[listNo].wind.speed);
                        dayObjects.dayFive.time.push(json.list[listNo].dt_txt);
                        break;
                }
            }
        };
    })

    return dayObjects;
}