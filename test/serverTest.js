var expect = require('chai').expect;
var request = require('request');

// tests for the express server
const url = 'http://localhost:3000/';

// checks whether the server is offering the correct root path
describe('Express Server', function () {

    describe('Root Path', function () {

        it('returns status 200', function () {
            request(url, function (error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns status 404', function () {
            request(url + 'fakePath', function (error, response, body) {
                expect(response.statusCode).to.equal(404);
            });
        });
    });
});