// tests for the api module 
import should from 'should';
import * as api from '../modules/api.mjs';

describe('API Testing', function () {

    // test for retrieving the json from an external api
    describe('fetchWeatherData', function () {

        it('returns json from an external api', function () {
            var cityToBeUsedAsTestInput = "London";
            let result = api.fetchWeatherData(cityToBeUsedAsTestInput);

            // checks the result for json type
            should(result).be.an.Object;
        });
    });
});