// tests for the dom script
import should from 'should';
import jsdomGlobal from 'jsdom-global';
import rewire from 'rewire';
import * as api from '../modules/api.mjs';
var dom = rewire('../modules/dom.js');

describe('Dom.js Testing', function () {

    // test for displaying given data
    describe('displayWeatherData', function () {

        // call method with wrong input
        // ignore alert error
        it('returns false if json is invalid', function () {
            var displayWeatherData = dom.__get__('displayWeatherData');
            var invalidJson = undefined;
            let result = displayWeatherData(invalidJson);

            // checks the result for falsehood
            result.should.be.false;
        });
    });

    // test for generating the object containing html div refs
    describe('getCollOfElements', function () {

        var getCollOfElements = dom.__get__('getCollOfElements');
        let result = getCollOfElements();

        it('returns an object', function () {

            // checks that the result is an object
            should(result).be.an.Object;
        });

        it('the object contains refs', function () {

            // checks that the object contains refs
            should(result).have.properties('dayOne', 'dayTwo', 'dayThree', 'dayFour', 'dayFive');
        });
    });

    // test for generating the object containing sorted json
    describe('sortJsonIntoDays', function () {

        it('return an object', function () {


            api.fetchWeatherData('London')
                .then(function (results) {

                    var sortJsonIntoDays = dom.__get__('sortJsonIntoDays');
                    var stringifiedJson = JSON.stringify(results);
                    var result = sortJsonIntoDays(stringifiedJson);

                    // checks that the result is an object
                    should(result).be.an.Object
                });
        });

        it('the object contains other objects', function () {});

        api.fetchWeatherData('London')
            .then(function (results) {

                var sortJsonIntoDays = dom.__get__('sortJsonIntoDays');
                var stringifiedJson = JSON.stringify(results);
                var result = sortJsonIntoDays(stringifiedJson);

                // checks that the object contains dayOne, dayTwo etc
                should(result).have.properties('dayOne', 'dayTwo', 'dayThree', 'dayFour', 'dayFive');
            });
    });
});