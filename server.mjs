// handles all server/express logic

// imports
import express from 'express';
import * as api from './modules/api';
import bodyParser from 'body-parser';

// const definitions
const app = express();
const port = 3000;

// static setup
app.use(express.static('views'));
app.use(express.static('assets'));
app.use(express.static('modules'));
app.use(express.static('test'));

// allows reading of requests
app.use(bodyParser.urlencoded({
    extended: true
}));

// allow usage of template engine
app.set('view engine', 'ejs');

// routing setup

// first page needs default data for ejs to function correctly
app.get('/', function (req, res) {
    res.render('pages/index', {
        data: "No weather data yet.",
    });
});

// handle display logic here
app.post('/', function (req, res) {

    var weatherData;

    // promise will resolve and deliver the fetched json
    api.fetchWeatherData(req.body.cityText)
        .then(function (results) {

            weatherData = results;

            // create dates to show on page
            var dayThree = new Date();
            var dayFour = new Date();
            var dayFive = new Date();

            dayThree.setDate(dayThree.getDate() + 2);
            dayFour.setDate(dayFour.getDate() + 3);
            dayFive.setDate(dayFive.getDate() + 4);

            // make it just the weekday
            var options = {
                weekday: 'long'
            };
            var dayThreeWord = new Intl.DateTimeFormat('en-UK', options).format(dayThree);
            var dayFourWord = new Intl.DateTimeFormat('en-UK', options).format(dayFour);
            var dayFiveWord = new Intl.DateTimeFormat('en-UK', options).format(dayFive);

            // render the same page with the new data from api
            setTimeout(function () {
                res.render('pages/index', {
                    body: req.body,
                    data: weatherData,
                    dateThree: dayThreeWord,
                    dateFour: dayFourWord,
                    dateFive: dayFiveWord,
                });
            }, 1000);
        });
});

// logging
app.listen(process.env.PORT || port);