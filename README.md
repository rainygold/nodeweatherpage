### Build
* Terminal command: node --experimental-modules server.mjs (from project dir)
* Tests: npm test (requires the previous command i.e. server running)
* Site: localhost:3000

### Tools
* OS: openSuse Tumbleweed
* Editor: Visual Studio Code/Emacs
* Node: 10.15.2
* Testing: Mocha
* Hosting: Heroku - https://node-weather-page.herokuapp.com/

### Ethos
A basic web site served by Express and Node. The user is able to input a city name and retrieve a five day forecast from today.

Project uses experimental modules flag for node to enable certain ES7 features.

Project structure follows a simple format of related directories. (e.g. views, assets)

### Testing
Unit tests were created for API interaction.

Uses Mocha, Chai, Request, Babel, Jsdom(global), Rewire, Should, Supertest modules.

### Improvements
* Code in dom.js could be refactored to be more compact/idiomatic. E.g. map instead of for-loop for appending data to HTML.
* More thorough testing through integration tests.
* Project was written with --experimental modules, writing with stable Node would be best for production.
* Front end (CSS/HTML) could follow a design theme or pattern.